const asyncHandler = require('../middleware/async');
const url = require('url');

const aws = require('aws-sdk');

aws.config.update({
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    accessKeyId: process.env.ACCESS_KEY_ID,
    region: 'ap-south-1'
});

const s3 = new aws.S3();

const addTemplate = async(req,res,next)=>{
    const params = {
        Bucket: 'apito-canvas/'+req.body.category,
        Key: Date.now() + "_" + req.files.image.name,
        Body: req.files.image.data,
        ContentType: req.files.image.mimetype, 
        ACL: 'public-read',
    };
    s3.upload(params, function(s3Err, data) {
        if (s3Err) throw s3Err
            console.log(`File uploaded successfully at ${data.Location}`);
        
        res.send(data);
    });

}

const getTemplates = asyncHandler(async(req,res,next)=> {
    const category = url.parse(req.url, true).query.category + '/';
    const params = {
        Bucket: 'apito-canvas',
        Prefix: category,
        Delimiter: '/'
    };

    s3.listObjectsV2(params, function(s3Err,data) {
        if (s3Err) throw s3Err
            console.log("Fetched successfully");
        
        res.send(data);
    })
})

const getAllTemplates = asyncHandler(async(req,res,next)=> {
    const params = {
        Bucket: 'apito-canvas'
    };

    s3.listObjectsV2(params, function(s3Err,data) {
        if (s3Err) throw s3Err
            console.log("Fetched successfully");
        
        res.send(data);
    })
})

const deleteTemplate = asyncHandler(async(req,res,next)=> {
    const key = url.parse(req.url, true).query.key;
    const params = {
        Bucket: 'apito-canvas',
        Key: key
    };

    s3.deleteObject(params, function(s3Err, data) {
        if (s3Err) 
            console.log(s3Err, s3Err.stack);  // error
        else     
            console.log("template Deleted");                 // deleted

        res.send(data);
    });
})

const deleteCategory = asyncHandler(async(req,res,next)=> {
    const category = url.parse(req.url, true).query.category + '/';
    const params = {
        Bucket: 'apito-canvas',
        Prefix: category,
        Delimiter: '/'
    };

    s3.listObjectsV2(params, function(s3Err,data) {
        if (s3Err) throw s3Err
            console.log("Fetched successfully");
        
        data.Contents.forEach((x, index, array) => {
            const templateParams = {
                Bucket: 'apito-canvas',
                Key: x.Key
            };

            s3.deleteObject(templateParams, function(s3Err, data) {
                if (s3Err) 
                    console.log(s3Err, s3Err.stack);  // error
                    
                    if (index + 1 === array.length ) {
                        res.send(data)
                    }
            })
        });
    })
})

module.exports = {
    addTemplate,
    getTemplates,
    deleteTemplate,
    deleteCategory,
    getAllTemplates
}