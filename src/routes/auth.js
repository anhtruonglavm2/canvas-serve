const express = require('express');
const {
    register, 
    login, 
    loggedInUser, 
    forgotPassword, 
    resetPassword, 
    updateDetails, 
    updatePassword, 
    logout,
    deactivateUser,
    deleteUser,
    toogleUserStatus,
    userList,
    getProjects,
    magicLogin
} = require('../controller/auth');
const { protect } = require('../middleware/auth');
const router = express.Router() 

router
    .post('/register', register)
router
    .post('/login',login)
router
    .post('/magicLogin',magicLogin)
router
    .get('/logout',logout)
router
    .post('/forgotpassword',forgotPassword)
router
    .put('/resetpassword/:resettoken', resetPassword)
router
    .put('/update', updateDetails)

router
    .put('/password', updatePassword)
router
    .get('/user', protect, loggedInUser)

router
    .put('/deactivate', deactivateUser)
router
    .delete('/deleteUser/:id', deleteUser)
router
    .put('/toogleUserStatus', toogleUserStatus)    
router
    .get('/userList', userList)
router
    .get('/projectList', getProjects)
module.exports = router;
