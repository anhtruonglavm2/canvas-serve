const express = require('express');
const router = express.Router();

const {
    addTemplate,
    getTemplates,
    getAllTemplates,
    deleteTemplate,
    deleteCategory
} = require('../controller/template')

router
    .post('/addTemplate', addTemplate)

router
    .get('/getTemplates', getTemplates)

router
    .get('/getAllTemplates', getAllTemplates)
router
    .delete('/deleteTemplate', deleteTemplate)

router
    .delete('/deleteCategory', deleteCategory)

module.exports = router;