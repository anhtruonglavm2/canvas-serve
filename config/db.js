const mongoose  = require('mongoose');

const connectDB = async () => {
    return await mongoose.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    } )
}

module.exports = connectDB